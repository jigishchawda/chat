package com.atlassian.chat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.atlassian.chat.helpers.FileUtil.inputStreamForFile;
import static com.atlassian.chat.util.InputStreamUtil.readFromStream;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HTMLPageFetcherTest {


    @Mock
    private URLConnection mockURLConnection;

    private HashMap<String, List<String>> headers;

    @Before
    public void beforeEveryTest() {
        headers = new HashMap<>();
    }

    @Test
    public void should_fetch_HTML_for_valid_url() throws IOException {
        String fileName = "htmlPage.html";
        String charset = "UTF-8";
        ClassLoader classLoader = getClass().getClassLoader();
        String expectedHtmlPage = readFromStream(inputStreamForFile(fileName, classLoader),
                Charset.forName(charset));
        headers.put("Content-Type", Collections.singletonList("text/html; charset=ISO-8859-1"));

        when(mockURLConnection.getInputStream()).thenReturn(inputStreamForFile(fileName, classLoader));
        when(mockURLConnection.getHeaderFields()).thenReturn(headers);

        URLStreamHandler handler = new URLStreamHandler() {
            @Override
            protected URLConnection openConnection(final URL arg0) throws IOException {
                return mockURLConnection;
            }
        };
        //cant mock final URL class. So inject mock objects in URL class
        final URL url = new URL("http://foo.bar", "foo.bar", 80, "", handler);

        String actualHtmlPage = new HTMLPageFetcher(url).fetch();

        assertThat(actualHtmlPage, is(expectedHtmlPage));

    }

    @Test
    public void should_return_null_for_non_html_content_type() throws IOException {
        when(mockURLConnection.getHeaderFieldKey(anyInt())).thenReturn("Content-Type");
        when(mockURLConnection.getHeaderField(anyInt())).thenReturn("json/application");
        URLStreamHandler handler = new URLStreamHandler() {
            @Override
            protected URLConnection openConnection(final URL arg0) throws IOException {
                return mockURLConnection;
            }
        };
        //cant mock final URL class. So inject mock objects in URL class
        final URL url = new URL("http://foo.bar", "foo.bar", 80, "", handler);

        String actualHtmlPage = new HTMLPageFetcher(url).fetch();

        assertNull(actualHtmlPage);

    }

}
