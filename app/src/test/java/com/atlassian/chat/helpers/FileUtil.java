package com.atlassian.chat.helpers;

import android.support.annotation.NonNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class FileUtil {

    @NonNull
    public static FileInputStream inputStreamForFile(String fileName, ClassLoader classLoader) throws FileNotFoundException {
        String file = classLoader.getResource(fileName).getFile();
        return new FileInputStream(new File(file));
    }

}
