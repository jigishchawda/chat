package com.atlassian.chat;

import com.atlassian.chat.models.specialcontent.Link;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.atlassian.chat.helpers.FileUtil.inputStreamForFile;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PageTitleFetcherCallableTest {

    @Mock
    URLConnection mockURLConnection;
    @Mock
    Link mockLink;

    private HashMap<String, List<String>> headers;

    @Before
    public void beforeEveryTest() {
        headers = new HashMap<>();
    }

    @Test
    public void should_return_link_with_page_title() throws IOException {
        String fileName = "htmlPage.html";
        ClassLoader classLoader = getClass().getClassLoader();
        headers.put("Content-Type", Collections.singletonList("text/html; charset=ISO-8859-1"));

        when(mockURLConnection.getInputStream()).thenReturn(inputStreamForFile(fileName, classLoader));
        when(mockURLConnection.getHeaderFields()).thenReturn(headers);
        URLStreamHandler handler = new URLStreamHandler() {
            @Override
            protected URLConnection openConnection(final URL arg0) throws IOException {
                return mockURLConnection;
            }
        };
        //cant mock final URL class. So inject mock objects in URL class
        final URL url = new URL("http://foo.bar", "foo.bar", 80, "", handler);
        when(mockLink.getUrl()).thenReturn(url);

        new PageTitleFetcherCallable(mockLink).call();

        verify(mockLink).setPageTitle("Some page title");

    }

    @Test
    public void should_not_call_set_page_title_on_link() throws MalformedURLException {
        doThrow(MalformedURLException.class).when(mockLink).getUrl();

        new PageTitleFetcherCallable(mockLink).call();

        verify(mockLink, never()).setPageTitle(anyString());
    }


}