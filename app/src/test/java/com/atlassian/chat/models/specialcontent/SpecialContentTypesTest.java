package com.atlassian.chat.models.specialcontent;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class SpecialContentTypesTest {
    @Test
    public void should_mine_mentions_from_message() {
        String message = "hi @John";
        MinedItems expectedMinedMentions = new MinedItems<>(singletonList(new Mention("John")));
        Map<String, MinedItems> expectedItemsMap = new HashMap<>();
        expectedItemsMap.put(SpecialContentTypes.MENTION_TYPE.key, expectedMinedMentions);

        Map<String, MinedItems> actualMinedItemsMap = SpecialContentTypes.mine(message);

        assertThat(actualMinedItemsMap, is(expectedItemsMap));
    }

    @Test
    public void should_mine_emoticons_from_message() {
        String message = "hi (smiley)";
        MinedItems expectedMinedEmoticons = new MinedItems<>(singletonList(new Emoticon("smiley")));
        Map<String, MinedItems> expectedMinedItemsMap = new HashMap<>();
        expectedMinedItemsMap.put(SpecialContentTypes.EMOTICON_TYPE.key, expectedMinedEmoticons);

        Map<String, MinedItems> actualMinedItemsMap = SpecialContentTypes.mine(message);

        assertThat(actualMinedItemsMap, is(expectedMinedItemsMap));
    }

    @Test
    public void should_mine_links_from_message() {
        String message = "check out today's doodle at http://www.google.com";
        MinedItems expectedMinedLinks = new MinedItems(singletonList(new Link("http://www.google.com")));
        Map<String, MinedItems> expectedMinedItemsMap = new HashMap<>();
        expectedMinedItemsMap.put(SpecialContentTypes.LINK_TYPE.key, expectedMinedLinks);

        Map<String, MinedItems> actualMinedItemsMap = SpecialContentTypes.mine(message);

        assertThat(actualMinedItemsMap, is(expectedMinedItemsMap));
    }

    @Test
    public void should_mine_link_emoticon_and_mention_from_message() {
        String message = "Hi @john (smiley). Check out today's doodle at http://www.google.com";
        MinedItems minedEmoticons = new MinedItems(Collections.singletonList(new Emoticon("smiley")));
        MinedItems minedMentions = new MinedItems(Collections.singletonList(new Mention("john")));
        MinedItems minedLinks = new MinedItems(Collections.singletonList(new Link("http://www.google.com")));
        Map<String, MinedItems> expectedMinedItemsMap = new HashMap<>();
        expectedMinedItemsMap.put(SpecialContentTypes.EMOTICON_TYPE.key, minedEmoticons);
        expectedMinedItemsMap.put(SpecialContentTypes.MENTION_TYPE.key, minedMentions);
        expectedMinedItemsMap.put(SpecialContentTypes.LINK_TYPE.key, minedLinks);

        Map<String, MinedItems> actualMinedItemsMap = SpecialContentTypes.mine(message);

        assertThat(actualMinedItemsMap, is(expectedMinedItemsMap));
    }

    @Test
    public void should_return_null_for_null_message() {
        assertNull(SpecialContentTypes.mine(null));
    }

    @Test
    public void should_return_null_for_empty_message() {
        assertNull(SpecialContentTypes.mine(""));
    }

    @Test
    public void should_return_empty_list_of_mined_items_for_no_special_content() {
        assertThat(SpecialContentTypes.mine("Hi. This is John"), CoreMatchers.<Map<String, MinedItems>>is(new HashMap<String, MinedItems>()));
    }


    @Test
    public void should_return_map_of_mined_items_from_json_string() throws IOException {
        String jsonString = "{\"mentions\": [\"john\", \"ram\"], \"emoticons\": [\"sunny\", \"happy\"], \"links\": [{\"url\":\"url1\", \"title\":\"title1\"},{\"url\":\"url2\", \"title\":\"title2\"}]}";
        Map<String, MinedItems> expectedMinedItemsMap = new HashMap<>();
        expectedMinedItemsMap.put("emoticons", new MinedItems(Arrays.asList(new Emoticon("sunny"), new Emoticon("happy"))));
        expectedMinedItemsMap.put("mentions", new MinedItems(Arrays.asList(new Mention("john"), new Mention("ram"))));
        Link link1 = new Link("url1"); link1.setPageTitle("title1");
        Link link2 = new Link("url2"); link2.setPageTitle("title1");
        expectedMinedItemsMap.put("links", new MinedItems(Arrays.asList(link1, link2)));

        Map<String, MinedItems> actualMinedItemsMap = SpecialContentTypes.createSpecialContentFromDB(jsonString);

        assertThat(actualMinedItemsMap, is(expectedMinedItemsMap));

    }

}