package com.atlassian.chat.models.specialcontent;

import com.fasterxml.jackson.core.JsonProcessingException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class SpecialContentTest {
    @Test
    public void should_return_true_for_mined_links() {
        HashMap<String, MinedItems> minedItemsMap = new HashMap<>();
        minedItemsMap.put(SpecialContentTypes.LINK_TYPE.key, new MinedItems(singletonList(new Link("some url"))));

        SpecialContent specialContent = new SpecialContent("someId", minedItemsMap);

        assertTrue(specialContent.hasLinks());
    }

    @Test
    public void should_return_false_for_no_mined_links() {
        SpecialContent specialContent = new SpecialContent("someId", new HashMap<String, MinedItems>());

        assertFalse(specialContent.hasLinks());
    }

    @Test
    public void should_return_mined_links() {
        HashMap<String, MinedItems> minedItemsMap = new HashMap<>();
        List<Link> expectedLinks = singletonList(new Link("some url"));
        minedItemsMap.put(SpecialContentTypes.LINK_TYPE.key, new MinedItems(expectedLinks));

        SpecialContent specialContent = new SpecialContent("someId", minedItemsMap);

        assertThat(specialContent.getLinks(), is(expectedLinks));

    }

    @Test
    public void should_return_json_string_for_mapped_content() throws JsonProcessingException, JSONException {
        Map<String, MinedItems> itemsMap = new HashMap<>();
        itemsMap.put(SpecialContentTypes.MENTION_TYPE.key, new MinedItems(Arrays.asList(new Mention("John"), new Mention("Oliver"))));
        itemsMap.put(SpecialContentTypes.EMOTICON_TYPE.key, new MinedItems(singletonList(new Emoticon("sunny"))));
        itemsMap.put(SpecialContentTypes.LINK_TYPE.key, new MinedItems(Arrays.asList(
                new Link("http://www.beautifulwallpapers.com"), new Link("http://www:idonnowhatiamdoing.com"))));

        SpecialContent specialContent = new SpecialContent("some id", itemsMap);
        String jsonString = specialContent.toJsonString();

        JSONObject jsonObject = new JSONObject(jsonString);

        assertLinks(jsonObject.getJSONArray("links"), Arrays.asList("http://www.beautifulwallpapers.com", "http://www:idonnowhatiamdoing.com"));
        assertMentions(jsonObject.getJSONArray("mentions"), Arrays.asList("John", "Oliver"));
        assertEmoticons(jsonObject.getJSONArray("emoticons"), Collections.singletonList("sunny"));
    }

    @Test
    public void should_return_special_content_from_json_from_DB() throws IOException {
        String jsonString = "{\"mentions\": [\"john\", \"ram\"], \"emoticons\": [\"sunny\", \"happy\"], \"links\": [{\"url\":\"url1\", \"title\":\"title1\"},{\"url\":\"url2\", \"title\":\"title2\"}]}";
        Map<String, MinedItems> expectedMinedItemsMap = new HashMap<>();
        expectedMinedItemsMap.put("emoticons", new MinedItems(Arrays.asList(new Emoticon("sunny"), new Emoticon("happy"))));
        expectedMinedItemsMap.put("mentions", new MinedItems(Arrays.asList(new Mention("john"), new Mention("ram"))));
        Link link1 = new Link("url1"); link1.setPageTitle("title1");
        Link link2 = new Link("url2"); link2.setPageTitle("title1");
        expectedMinedItemsMap.put("links", new MinedItems(Arrays.asList(link1, link2)));
        SpecialContent expectedSpecialContent = new SpecialContent("someId", expectedMinedItemsMap);

        SpecialContent actualSpecialContent = SpecialContent.fromJsonString(jsonString, "someId");

        assertThat(actualSpecialContent, is(expectedSpecialContent));
    }


    private void assertEmoticons(JSONArray emoticons, List<String> expectedEmoticons) throws JSONException {
        for (int i = 0; i < emoticons.length(); i++) {
            assertTrue(expectedEmoticons.contains(emoticons.getString(i)));
        }
    }

    private void assertMentions(JSONArray mentions, List<String> expectedMentions) throws JSONException {
        for (int i = 0; i < mentions.length(); i++) {
            assertTrue(expectedMentions.contains(mentions.getString(i)));
        }
    }

    private void assertLinks(JSONArray actualLinks, List<String> expectedLinks) throws JSONException {
        for (int i = 0; i < actualLinks.length(); i++) {
            JSONObject linkJson = actualLinks.getJSONObject(i);
            assertTrue(expectedLinks.contains(linkJson.getString("url")));
            assertThat(linkJson.getString("title"), is("NA"));
        }
    }

}