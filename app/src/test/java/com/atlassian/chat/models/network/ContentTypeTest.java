package com.atlassian.chat.models.network;

import org.junit.Test;

import java.nio.charset.Charset;

import static java.nio.charset.Charset.forName;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ContentTypeTest {
    @Test
    public void should_have_valid_content_type_value() {
        ContentType contentType = new ContentType("text/html;charset=UTF-8");

        assertTrue(contentType.is("text/html"));
    }

    @Test
    public void should_have_valid_content_type_charset() {
        ContentType contentType = new ContentType("text/html;charset=UTF-8");

        assertThat(contentType.getCharset(), is(forName("UTF-8")));
    }

    @Test(expected=IllegalArgumentException.class)
    public void should_throw_exception_for_null_header_value() {
        new ContentType(null);
    }

    @Test
    public void should_return_default_charset_for_no_charset_in_content_type() {
        ContentType contentType = new ContentType("text/html");

        assertThat(contentType.getCharset(), is(Charset.defaultCharset()));
    }

    @Test
    public void should_return_default_charset_for_invalid_charset_in_content_type() {
        ContentType contentType = new ContentType("text/html;charset=invalid");

        assertThat(contentType.getCharset(), is(Charset.defaultCharset()));
    }


}