package com.atlassian.chat.models.specialcontent;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SpecialContentMentionTypeTest {
    private SpecialContentTypes mentionType = SpecialContentTypes.MENTION_TYPE;

    @Test
    public void should_mine_one_mention() {
        String message = "@chris you around?";
        List<Mention> expectedMentions = Collections.singletonList(new Mention("chris"));

        MinedItems<Mention> content = mentionType.mineFrom(message);
        List<Mention> actualMentions = content.getMinedItems();

        assertThat(actualMentions.size(), is(1));
        assertThat(actualMentions, is(expectedMentions));
    }

    @Test
    public void should_mine_two_mentions() {
        String message = "@chris and @john, lets start the meeting.";
        List<Mention> expectedMentions = Arrays.asList(new Mention("chris"), new Mention("john"));

        MinedItems<Mention> minedContent = mentionType.mineFrom(message);
        List<Mention> actualMentions = minedContent.getMinedItems();

        assertThat(actualMentions.size(), is(2));
        assertThat(actualMentions, is(expectedMentions));
    }

    @Test
    public void should_mine_mention_prefixed_by_non_word_character() {
        String message = "Task is assigned to:@chris";
        List<Mention> expectedMentions = Collections.singletonList(new Mention("chris"));

        MinedItems<Mention> content = mentionType.mineFrom(message);
        List<Mention> actualMentions = content.getMinedItems();

        assertThat(actualMentions, is(expectedMentions));
    }

    @Test
    public void should_mine_mention_suffixed_by_non_word_character() {
        String message = "Good morning @chris!!";
        List<Mention> expectedMentions = Collections.singletonList(new Mention("chris"));

        MinedItems<Mention> content = mentionType.mineFrom(message);
        List<Mention> actualMentions = content.getMinedItems();

        assertThat(actualMentions, is(expectedMentions));
    }

    @Test
    public void should_return_empty_list_of_mentions_for_empty_message() {
        String message = "";
        List<Mention> expectedMentions = Collections.EMPTY_LIST;

        MinedItems<Mention> content = mentionType.mineFrom(message);
        List<Mention> actualMentions = content.getMinedItems();

        assertThat(actualMentions, is(expectedMentions));
    }

    @Test
    public void should_return_empty_list_of_mentions_for_null_message() {
        List<Mention> expectedMentions = Collections.EMPTY_LIST;

        MinedItems<Mention> content = mentionType.mineFrom(null);
        List<Mention> actualMentions = content.getMinedItems();

        assertThat(actualMentions, is(expectedMentions));
    }

}