package com.atlassian.chat.models;

import com.atlassian.chat.models.specialcontent.Emoticon;
import com.atlassian.chat.models.specialcontent.Link;
import com.atlassian.chat.models.specialcontent.Mention;
import com.atlassian.chat.models.specialcontent.MinedItems;
import com.atlassian.chat.models.specialcontent.SpecialContent;
import com.atlassian.chat.models.specialcontent.SpecialContentTypes;
import com.fasterxml.jackson.core.JsonProcessingException;

import org.junit.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.chat.models.ChatMessage.MessageType.OUTGOING_MESSAGE;
import static java.util.Collections.singletonList;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class ChatMessageTest {

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_exception_for_null_message() {
        new ChatMessage(null, null, new Date().getTime());
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_exception_for_empty_message() {
        new ChatMessage("", OUTGOING_MESSAGE, new Date().getTime());
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_exception_for_null_message_type() {
        new ChatMessage("some message", null, new Date().getTime());
    }

    @Test
    public void should_return_special_content_on_mining_special_items() throws JsonProcessingException {
        String message = "Hi @John and @Oliver. Its a (sunny) outside. check http://www.beautifulwallpapers.com" +
                " and http://www:idonnowhatiamdoing.com";
        ChatMessage chatMessage = new ChatMessage(message, OUTGOING_MESSAGE, new Date().getTime());
        Map<String, MinedItems> itemsMap = new HashMap<>();
        itemsMap.put(SpecialContentTypes.MENTION_TYPE.key, new MinedItems(Arrays.asList(new Mention("John"), new Mention("Oliver"))));
        itemsMap.put(SpecialContentTypes.EMOTICON_TYPE.key, new MinedItems(singletonList(new Emoticon("sunny"))));
        itemsMap.put(SpecialContentTypes.LINK_TYPE.key, new MinedItems(Arrays.asList(
                new Link("http://www.beautifulwallpapers.com"), new Link("http://www:idonnowhatiamdoing.com"))));
        SpecialContent expectedSpecialContent = new SpecialContent(chatMessage.id, itemsMap);

        SpecialContent actualSpecialContent = chatMessage.getSpecialContent();

        assertThat(actualSpecialContent, is(expectedSpecialContent));

    }

    @Test
    public void should_return_no_special_content_for_plain_message() {
        ChatMessage chatMessage = new ChatMessage("Hi John. Its a sunny weather.", OUTGOING_MESSAGE, new Date().getTime());

        SpecialContent specialContent = chatMessage.getSpecialContent();

        assertThat(specialContent, is(new SpecialContent(chatMessage.id, new HashMap<String, MinedItems>())));
    }


}