package com.atlassian.chat.models.specialcontent;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class PageTitleMinerTest {
    @Test
    public void should_mine_title_from_html() {
        String htmlString = "<html>" +
                "<head>Page heading</head>" +
                "<title>Some title</title>" +
                "<body></body>" +
                "</html>";
        String expectedPageTitle = "Some title";

        String actualPageTitle = PageTitleMiner.mine(htmlString);

        assertThat(actualPageTitle, is(expectedPageTitle));

    }

    @Test
    public void should_return_null_for_no_title_in_html() {
        String htmlString = "<html>" +
                "<head>Page heading</head>" +
                "<body></body>" +
                "</html>";

        String actualPageTitle = PageTitleMiner.mine(htmlString);

        assertNull(actualPageTitle);
    }


    @Test
    public void should_return_null_for_null_html() {
        assertNull(PageTitleMiner.mine(null));
    }

}
