package com.atlassian.chat.models.specialcontent;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SpecialContentLinkTypeTest {
    private SpecialContentTypes linkType = SpecialContentTypes.LINK_TYPE;

    @Test
    public void should_mine_one_link() {
        String message = "Look at this url: http://example.com";
        List<Link> expectedLinks = Collections.singletonList(new Link("http://example.com"));

        MinedItems<Link> minedLinks = linkType.mineFrom(message);
        List<Link> actualLinks = minedLinks.getMinedItems();

        assertThat(actualLinks.size(), is(1));
        assertThat(actualLinks, is(expectedLinks));
    }

    @Test
    public void should_mine_two_links() {
        String message = "Look at http://example.com and https://www.another-example.com";
        List<Link> expectedLinks = Arrays.asList(new Link("http://example.com"), new Link("https://www.another-example.com"));

        MinedItems<Link> minedLinks = linkType.mineFrom(message);
        List<Link> actualLinks = minedLinks.getMinedItems();

        assertThat(actualLinks.size(), is(2));
        assertThat(actualLinks, is(expectedLinks));
    }

    @Test
    public void should_not_mine_for_link_not_starting_with_http_or_https() {
        String message = "Look at httpd://example.com";
        List<Link> expectedLinks = Collections.EMPTY_LIST;

        MinedItems<Link> minedLinks = linkType.mineFrom(message);
        List<Link> actualLinks = minedLinks.getMinedItems();

        assertThat(actualLinks, is(expectedLinks));
    }

    @Test
    public void should_not_mine_for_null_message() {
        List<Link> expectedLinks = Collections.EMPTY_LIST;

        MinedItems<Link> minedLinks = linkType.mineFrom(null);
        List<Link> actualLinks = minedLinks.getMinedItems();

        assertThat(actualLinks, is(expectedLinks));
    }

    @Test
    public void should_not_mine_for_empty_message() {
        String message = "";
        List<Link> expectedLinks = Collections.EMPTY_LIST;

        MinedItems<Link> minedLinks = linkType.mineFrom(message);
        List<Link> actualLinks = minedLinks.getMinedItems();

        assertThat(actualLinks, is(expectedLinks));
    }


}