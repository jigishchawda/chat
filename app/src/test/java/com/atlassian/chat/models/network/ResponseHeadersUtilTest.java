package com.atlassian.chat.models.network;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URLConnection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ResponseHeadersUtilTest {


    @Mock
    URLConnection mockURLConnection;

    private HashMap<String, List<String>> headers;

    @Before
    public void beforeEveryTest() {
        headers = new HashMap<>();
    }

    @Test
    public void should_return_valid_content_type() {
        headers.put("Date", Collections.singletonList("Thu, 31 Dec 2015 05:57:04 GMT"));
        headers.put("Expires", Collections.singletonList("-1"));
        headers.put("Content-Type", Collections.singletonList("text/html; charset=ISO-8859-1"));

        when(mockURLConnection.getHeaderFields()).thenReturn(headers);
        ContentType expectedContentType = new ContentType("text/html; charset=ISO-8859-1");

        ContentType contentTypeHeader = ResponseHeadersUtil.getContentTypeHeader(mockURLConnection);

        assertThat(contentTypeHeader, is(expectedContentType));

    }
    @Test
    public void should_return_null_for_missing_content_type() {
        headers.put("Date", Collections.singletonList("Thu, 31 Dec 2015 05:57:04 GMT"));
        headers.put("Expires", Collections.singletonList("-1"));

        when(mockURLConnection.getHeaderFields()).thenReturn(headers);

        ContentType contentTypeHeader = ResponseHeadersUtil.getContentTypeHeader(mockURLConnection);

        assertNull(contentTypeHeader);

    }

    @Test
    public void should_return_null_for_null_content_type() {
        headers.put("Date", Collections.singletonList("Thu, 31 Dec 2015 05:57:04 GMT"));
        headers.put("Expires", Collections.singletonList("-1"));
        headers.put("Content-Type", null);
        when(mockURLConnection.getHeaderFields()).thenReturn(headers);

        assertNull(ResponseHeadersUtil.getContentTypeHeader(mockURLConnection));
    }


}