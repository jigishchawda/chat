package com.atlassian.chat.models.specialcontent;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SpecialContentEmoticonTypeTest {
    private SpecialContentTypes emoticonType = SpecialContentTypes.EMOTICON_TYPE;

    @Test
    public void should_mine_one_emoticon() {
        String message = "Good morning! (megusta)";
        List<Emoticon> expectedEmoticons = Collections.singletonList(new Emoticon("megusta"));

        MinedItems<Emoticon> minedEmoticons = emoticonType.mineFrom(message);
        List<Emoticon> actualEmoticons = minedEmoticons.getMinedItems();

        assertThat(actualEmoticons.size(), is(1));
        assertThat(actualEmoticons, is(expectedEmoticons));
    }

    @Test
    public void should_mine_two_emoticons() {
        String message = "Good morning! (megusta). (coffee)?";
        List<Emoticon> expectedEmoticons = Arrays.asList(new Emoticon("megusta"), new Emoticon("coffee"));

        MinedItems<Emoticon> minedEmoticons = emoticonType.mineFrom(message);
        List<Emoticon> actualEmoticons = minedEmoticons.getMinedItems();

        assertThat(actualEmoticons.size(), is(2));
        assertThat(actualEmoticons, is(expectedEmoticons));
    }

    @Test
    public void should_return_empty_list_of_emoticons_for_blank_message() {
        String message = "";
        List<Emoticon> expectedEmoticons = Collections.EMPTY_LIST;

        MinedItems<Emoticon> minedEmoticons = emoticonType.mineFrom(message);
        List<Emoticon> actualEmoticons = minedEmoticons.getMinedItems();

        assertThat(actualEmoticons, is(expectedEmoticons));
    }

    @Test
    public void should_return_empty_list_of_emoticons_for_null_message() {
        List<Emoticon> expectedEmoticons = Collections.EMPTY_LIST;

        MinedItems<Emoticon> minedEmoticons = emoticonType.mineFrom(null);
        List<Emoticon> actualEmoticons = minedEmoticons.getMinedItems();

        assertThat(actualEmoticons, is(expectedEmoticons));
    }

    @Test
    public void should_not_mine_emoticon_from_empty_paranthesis() {
        String message = "I am using toString() function.";
        List<Emoticon> expectedEmoticons = Collections.EMPTY_LIST;

        MinedItems<Emoticon> minedEmoticons = emoticonType.mineFrom(message);
        List<Emoticon> actualEmoticons = minedEmoticons.getMinedItems();

        assertThat(actualEmoticons, is(expectedEmoticons));
    }

    @Test
    public void should_not_mine_emoticon_for_more_than_fifteen_characters_in_paranthesis() {
        String message = "(moreThanFifteenCharacters) is not an emoticon";
        List<Emoticon> expectedEmoticons = Collections.EMPTY_LIST;

        MinedItems<Emoticon> minedEmoticons = emoticonType.mineFrom(message);
        List<Emoticon> actualEmoticons = minedEmoticons.getMinedItems();

        assertThat(actualEmoticons, is(expectedEmoticons));
    }

    @Test
    public void should_mine_emoticon_for_exactly_fifteen_characters_in_paranthesis() {
        String message = "(abcdefghijklmno) is an emoticon";
        List<Emoticon> expectedEmoticons = Collections.singletonList(new Emoticon("abcdefghijklmno"));

        MinedItems<Emoticon> minedEmoticons = emoticonType.mineFrom(message);
        List<Emoticon> actualEmoticons = minedEmoticons.getMinedItems();

        assertThat(actualEmoticons, is(expectedEmoticons));
    }

    @Test
    public void should_mine_only_alphanumeric_characters_in_paranthesis() {
        String message = "(only15character) is an emoticon";
        List<Emoticon> expectedEmoticons = Collections.singletonList(new Emoticon("only15character"));

        MinedItems<Emoticon> minedEmoticons = emoticonType.mineFrom(message);
        List<Emoticon> actualEmoticons = minedEmoticons.getMinedItems();

        assertThat(actualEmoticons, is(expectedEmoticons));
    }

    @Test
    public void should__not_mine_non_alphanumeric_characters_in_paranthesis() {
        String message = "(digit_character) is not an emoticon";
        List<Emoticon> expectedEmoticons = Collections.EMPTY_LIST;

        MinedItems<Emoticon> minedEmoticons = emoticonType.mineFrom(message);
        List<Emoticon> actualEmoticons = minedEmoticons.getMinedItems();

        assertThat(actualEmoticons, is(expectedEmoticons));
    }




}