package com.atlassian.chat.models;

import com.atlassian.chat.HTMLPageFetcher;
import com.atlassian.chat.HTMLPageTitleFetcher;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.nio.charset.Charset;

import static com.atlassian.chat.helpers.FileUtil.inputStreamForFile;
import static com.atlassian.chat.util.InputStreamUtil.readFromStream;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HTMLPageTitleFetcherTest {
    @Mock
    HTMLPageFetcher mockPageFetcher;
    @Test
    public void should_return_html_page_title() throws IOException {
        String fileName = "htmlPage.html";
        String expectedPageTitle = "Some page title";
        String htmlPage = readFromStream(inputStreamForFile(fileName, getClass().getClassLoader()),
                Charset.forName("UTF-8"));
        when(mockPageFetcher.fetch()).thenReturn(htmlPage);

        HTMLPageTitleFetcher htmlPageTitleFetcher = new HTMLPageTitleFetcher(mockPageFetcher);
        String actualPageTitle = htmlPageTitleFetcher.fetch();

        assertThat(actualPageTitle, is(expectedPageTitle));

    }

    @Test
    public void should_return_null_for_invalid_html_page() throws IOException {
        when(mockPageFetcher.fetch()).thenReturn(null);

        HTMLPageTitleFetcher htmlPageTitleFetcher = new HTMLPageTitleFetcher(mockPageFetcher);
        String actualPageTitle = htmlPageTitleFetcher.fetch();

        assertNull(actualPageTitle);
    }


}
