package com.atlassian.chat.db.operations;

import android.content.Context;
import android.os.AsyncTask;

import com.atlassian.chat.db.DatabaseHelper;
import com.atlassian.chat.db.DatabaseResultCallback;

public class MessagesCountAsynTask extends AsyncTask<Void, Void, Long>{
    private final DatabaseHelper database;
    private DatabaseResultCallback<Long> resultCallback;

    public MessagesCountAsynTask(Context context, DatabaseResultCallback<Long> resultCallback) {
        this.resultCallback = resultCallback;
        this.database = new DatabaseHelper(context);
    }

    @Override
    protected Long doInBackground(Void... params) {
        return database.getMessagesCount();
    }

    @Override
    protected void onPostExecute(Long messagesCount) {
        resultCallback.execute(messagesCount);
    }
}
