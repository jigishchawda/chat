package com.atlassian.chat.db.operations;

import android.content.Context;
import android.os.AsyncTask;

import com.atlassian.chat.db.DatabaseHelper;
import com.atlassian.chat.db.DatabaseResultCallback;
import com.atlassian.chat.models.specialcontent.Link;

import java.util.List;

public class IncomingMessageWithPageTitleAddAsyncTask extends AsyncTask<String, Void, String> {
    private final DatabaseHelper database;
    private List<Link> links;
    private DatabaseResultCallback<String> databaseResultCallback;

    public IncomingMessageWithPageTitleAddAsyncTask(Context context, List<Link> links, DatabaseResultCallback<String> databaseResultCallback) {
        this.links = links;
        this.databaseResultCallback = databaseResultCallback;
        this.database = new DatabaseHelper(context);
    }

    @Override
    protected String doInBackground(String... params) {
        String chatMessageId = params[0];
        database.updateLinksForMessage(chatMessageId, links);
        return database.addIncmoingMessageFor(chatMessageId);
    }

    @Override
    protected void onPostExecute(String incomingMessageId) {
        databaseResultCallback.execute(incomingMessageId);
    }
}
