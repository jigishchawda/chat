package com.atlassian.chat.db;

public interface DatabaseResultCallback<R> {
    void execute(R result);
}
