package com.atlassian.chat.db.operations;

import android.content.Context;
import android.os.AsyncTask;

import com.atlassian.chat.db.DatabaseHelper;
import com.atlassian.chat.db.DatabaseResultCallback;
import com.atlassian.chat.models.ChatMessage;

import java.util.List;

public class MessagesFetchAsyncTask extends AsyncTask<Void, Void, List<ChatMessage>> {
    private final DatabaseResultCallback callback;
    private final DatabaseHelper database;

    public MessagesFetchAsyncTask(Context context, DatabaseResultCallback callback) {
        this.callback = callback;
        this.database = new DatabaseHelper(context);
    }

    @Override
    protected List<ChatMessage> doInBackground(Void... params) {
        return database.readAllMessages();
    }

    @Override
    protected void onPostExecute(List<ChatMessage> chatMessages) {
        callback.execute(chatMessages);
    }
}
