package com.atlassian.chat.db.operations;

import android.content.Context;
import android.os.AsyncTask;

import com.atlassian.chat.db.DatabaseHelper;
import com.atlassian.chat.db.DatabaseResultCallback;
import com.atlassian.chat.models.ChatMessage;

public class MessageFetchByIdAsyncTask extends AsyncTask<String, Void, ChatMessage> {
    private final DatabaseResultCallback callback;
    private final DatabaseHelper database;

    public MessageFetchByIdAsyncTask(Context context, DatabaseResultCallback callback) {
        this.callback = callback;
        this.database = new DatabaseHelper(context);
    }

    @Override
    protected ChatMessage doInBackground(String... params) {
        String messageId = params[0];
        return database.readChatMessageById(messageId);
    }

    @Override
    protected void onPostExecute(ChatMessage chatMessage) {
        if(callback != null) callback.execute(chatMessage);
    }

}
