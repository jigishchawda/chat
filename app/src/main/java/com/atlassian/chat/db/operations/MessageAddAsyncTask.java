package com.atlassian.chat.db.operations;

import android.content.Context;
import android.os.AsyncTask;

import com.atlassian.chat.db.DatabaseHelper;
import com.atlassian.chat.models.ChatMessage;

public class MessageAddAsyncTask extends AsyncTask<ChatMessage, Void, Void> {
    private final DatabaseHelper database;

    public MessageAddAsyncTask(Context context) {
        this.database = new DatabaseHelper(context);
    }

    @Override
    protected Void doInBackground(ChatMessage... params) {
        ChatMessage chatMessage = params[0];
        database.addChatMessage(chatMessage);
        return null;
    }
}
