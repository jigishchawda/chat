package com.atlassian.chat.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import com.atlassian.chat.models.ChatMessage;
import com.atlassian.chat.models.specialcontent.Link;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "chatmessages.db";
    private static final int DATABASE_VERSION = 1;
    private static final String MESSAGES_TABLE = "messages";
    private static final String KEY_ID = "id";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_TIMESTAMP = "timestamp";
    private static final String KEY_MESSAGE_TYPE = "messagetype";
    private static final String KEY_SPECIAL_CONTENT = "specialcontent";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_MESSAGES_TABLE = "CREATE TABLE " + MESSAGES_TABLE + "("
                + KEY_ID + " TEXT PRIMARY KEY,"
                + KEY_MESSAGE + " TEXT NOT NULL,"
                + KEY_TIMESTAMP + " TEXT NOT NULL,"
                + KEY_MESSAGE_TYPE + " INTEGER NOT NULL,"
                + KEY_SPECIAL_CONTENT + " TEXT" + ")";
        db.execSQL(CREATE_MESSAGES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_NAME);

        // Create tables again
        onCreate(db);
    }

    public void addChatMessage(ChatMessage chatMessage) {
        SQLiteDatabase database = getWritableDatabase();
        addChatMessage(chatMessage, database);
        database.close();
    }

    private void addChatMessage(ChatMessage chatMessage, SQLiteDatabase database) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_ID, chatMessage.id);
        contentValues.put(KEY_MESSAGE, chatMessage.message);
        contentValues.put(KEY_TIMESTAMP, String.valueOf(chatMessage.timestamp));
        contentValues.put(KEY_MESSAGE_TYPE, chatMessage.messageType.ordinal());
        contentValues.put(KEY_SPECIAL_CONTENT, chatMessage.getSpecialContentJsonString());

        long insert = database.insert(MESSAGES_TABLE, null, contentValues);
        if (insert == -1) {
            System.out.println("The record is not inserted");
        }
    }

    public ChatMessage readChatMessageById(String messageId) {
        SQLiteDatabase database = getReadableDatabase();
        try {
            return getChatMessageById(messageId, database);
        } finally {
            database.close();
        }
    }

    @Nullable
    private ChatMessage getChatMessageById(String messageId, SQLiteDatabase database) {
        Cursor cursor = database.query(MESSAGES_TABLE, new String[]{KEY_ID, KEY_MESSAGE, KEY_MESSAGE_TYPE, KEY_SPECIAL_CONTENT, KEY_TIMESTAMP}
                , KEY_ID + "=?", new String[]{messageId}, null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            String id = cursor.getString(0);
            String message = cursor.getString(1);
            int messagetypeOrdinal = cursor.getInt(2);
            String specialContentJsonString = cursor.getString(3);
            String timestampString = cursor.getString(4);
            cursor.close();
            return ChatMessage.fromDBValues(id, message, timestampString, messagetypeOrdinal, specialContentJsonString);
        } else return null;
    }

    public List<ChatMessage> readAllMessages() {
        SQLiteDatabase database = getReadableDatabase();
        String query = "SELECT * from " + MESSAGES_TABLE;
        List<ChatMessage> chatMessages = new ArrayList<>();
        try {
            Cursor cursor = database.rawQuery(query, null);
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    String id = cursor.getString(0);
                    String message = cursor.getString(1);
                    String timestampString = cursor.getString(2);
                    int messageTypeOrdinal = cursor.getInt(3);
                    String specialContentJsonString = cursor.getString(4);
                    ChatMessage chatMessage = ChatMessage.fromDBValues(id, message, timestampString, messageTypeOrdinal
                            , specialContentJsonString);
                    chatMessages.add(chatMessage);
                } while (cursor.moveToNext());
                cursor.close();
            }
        } finally {
            database.close();
        }
        return chatMessages;
    }

    public void updateLinksForMessage(String messageId, List<Link> links) {
        SQLiteDatabase database = getWritableDatabase();
        try {
            ChatMessage chatMessage = getChatMessageById(messageId, database);
            if(chatMessage == null) return;
            chatMessage.updateLinksWithPageTitles(links);
            ContentValues values = new ContentValues();
            values.put(KEY_SPECIAL_CONTENT, chatMessage.getSpecialContentJsonString());
            database.update(MESSAGES_TABLE, values, KEY_ID + "='" + chatMessage.id + "'", null);
        } finally {
            database.close();
        }

    }

    public String addIncmoingMessageFor(String chatMessageId) {
        SQLiteDatabase database = getWritableDatabase();
        try {
            ChatMessage chatMessage = getChatMessageById(chatMessageId, database);
            if(chatMessage == null) return null;
            ChatMessage incomingMessage = ChatMessage.createIncomingMessage(chatMessage.getSpecialContentJsonString());
            addChatMessage(incomingMessage, database);
            return incomingMessage.id;
        } finally {
            database.close();
        }
    }

    public Long getMessagesCount() {
        SQLiteDatabase database = getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(database, MESSAGES_TABLE);
        database.close();
        return count;
    }
}
