package com.atlassian.chat;

import com.atlassian.chat.models.specialcontent.Link;

import java.util.List;

public interface IHTMLPageTitleFetcher {
    void fetchPageTitlesFor(List<Link> links, String messageId);
}
