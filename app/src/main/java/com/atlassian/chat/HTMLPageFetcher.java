package com.atlassian.chat;

import com.atlassian.chat.models.network.ContentType;
import com.atlassian.chat.models.network.ResponseHeadersUtil;

import java.net.URL;
import java.net.URLConnection;

import static com.atlassian.chat.util.InputStreamUtil.readFromStream;

public class HTMLPageFetcher {

    private static final String TEXT_HTML_CONTENT_TYPE = "text/html";
    private final URL url;

    public HTMLPageFetcher(URL url) {
        this.url = url;
    }

    public String fetch() {
        try{
            URLConnection urlConnection = url.openConnection();
            ContentType contentTypeHeader = ResponseHeadersUtil.getContentTypeHeader(urlConnection);
            if(isContentTypeNonHtml(contentTypeHeader)) {
                return null;
            } else {
                return readFromStream(urlConnection.getInputStream(), contentTypeHeader.getCharset());
            }
        } catch (Exception exception){
            return null;
        }

    }



    private static boolean isContentTypeNonHtml(ContentType contentTypeHeader) {
        return !(contentTypeHeader != null && contentTypeHeader.is(TEXT_HTML_CONTENT_TYPE));
    }

}
