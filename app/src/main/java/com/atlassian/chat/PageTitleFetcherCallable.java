package com.atlassian.chat;

import com.atlassian.chat.models.specialcontent.Link;

import java.net.MalformedURLException;
import java.util.concurrent.Callable;

public class PageTitleFetcherCallable implements Callable<Link> {
    private final Link link;

    public PageTitleFetcherCallable(Link link) {
        this.link = link;
    }

    @Override
    public Link call() {
        try {
            HTMLPageFetcher htmlPageFetcher = new HTMLPageFetcher(link.getUrl());
            HTMLPageTitleFetcher htmlPageTitleFetcher = new HTMLPageTitleFetcher(htmlPageFetcher);
            link.setPageTitle(htmlPageTitleFetcher.fetch());
            return link;
        } catch (MalformedURLException e) {
            return link;
        }
    }
}
