package com.atlassian.chat;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;

import com.atlassian.chat.db.DatabaseResultCallback;
import com.atlassian.chat.db.operations.IncomingMessageWithPageTitleAddAsyncTask;
import com.atlassian.chat.models.specialcontent.Link;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static com.atlassian.chat.models.ChatMessage.OUTGOING_MESSAGE_ID_EXTRAS;
import static com.atlassian.chat.models.ChatMessage.INCOMING_MESSAGE_ID_EXTRAS;
import static com.atlassian.chat.models.specialcontent.Link.LINKS_EXTRAS;

public class PageTitleFetcherIntentService extends IntentService {
    private static final int MAX_THREADS = 3;
    public static final String PAGE_TITLES_FETCHED = "com.atlassian.chat.action.pageTitlesFetched";

    public PageTitleFetcherIntentService() {
        super("page-title-fetcher");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        ExecutorService executorService = Executors.newFixedThreadPool(MAX_THREADS);

        ArrayList<Link> links = intent.getParcelableArrayListExtra(LINKS_EXTRAS);
        String outgoingMessageId = intent.getStringExtra(OUTGOING_MESSAGE_ID_EXTRAS);
        Set<Callable<Link>> callables = getPageTitleFetcherCallables(links);

        ArrayList<Link> linksWithPageTitle = getLinksWithPageTitles(executorService, callables);
        executorService.shutdown();

        addIncomingMessageWithUpdatesLinksToDatabase(linksWithPageTitle, outgoingMessageId);
    }

    private void addIncomingMessageWithUpdatesLinksToDatabase(final List<Link> linksWithPageTitle, final String outgoingMessageId) {
        new IncomingMessageWithPageTitleAddAsyncTask(PageTitleFetcherIntentService.this, linksWithPageTitle, new DatabaseResultCallback<String>() {
            @Override
            public void execute(String incomingMessageId) {
                notifyPageTitlesFetched(incomingMessageId);
            }
        }).execute(outgoingMessageId);
    }

    @NonNull
    private ArrayList<Link> getLinksWithPageTitles(ExecutorService executorService, Set<Callable<Link>> callables) {
        ArrayList<Link> linksWithPageTitle = new ArrayList<>();
        try {
            List<Future<Link>> futures = executorService.invokeAll(callables);
            for (Future<Link> future : futures) {
                linksWithPageTitle.add(future.get());
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return linksWithPageTitle;
    }

    private void notifyPageTitlesFetched(String incomingMessageId) {
        Intent fetchIntent = new Intent(PAGE_TITLES_FETCHED);
        fetchIntent.putExtra(INCOMING_MESSAGE_ID_EXTRAS, incomingMessageId);
        LocalBroadcastManager.getInstance(this).sendBroadcast(fetchIntent);
    }

    @NonNull
    private Set<Callable<Link>> getPageTitleFetcherCallables(ArrayList<Link> links) {
        Set<Callable<Link>> callables = new HashSet<>();
        for (final Link link : links) {
            callables.add(new PageTitleFetcherCallable(link));
        }
        return callables;
    }


}
