package com.atlassian.chat;

import com.atlassian.chat.models.specialcontent.PageTitleMiner;

public class HTMLPageTitleFetcher {
    private final HTMLPageFetcher htmlPageFetcher;

    public HTMLPageTitleFetcher(HTMLPageFetcher htmlPageFetcher) {
        this.htmlPageFetcher = htmlPageFetcher;
    }

    public String fetch() {
        return PageTitleMiner.mine(htmlPageFetcher.fetch());
    }
}
