package com.atlassian.chat.models.network;

import java.net.URLConnection;
import java.util.List;
import java.util.Map;

public class ResponseHeadersUtil {


    public static ContentType getContentTypeHeader(URLConnection conn) {
        try {
            Map<String, List<String>> headerFields = conn.getHeaderFields();
            if(headerFields.containsKey("Content-Type")) {
                List<String> contentTypes = headerFields.get("Content-Type");
                return new ContentType(contentTypes.get(0));
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }
}
