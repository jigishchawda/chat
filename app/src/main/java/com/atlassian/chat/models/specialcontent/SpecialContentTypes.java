package com.atlassian.chat.models.specialcontent;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.chat.models.specialcontent.SpecialContentPatterns.EMOTICON_GROUP;
import static com.atlassian.chat.models.specialcontent.SpecialContentPatterns.EMOTICON_PATTERN;
import static com.atlassian.chat.models.specialcontent.SpecialContentPatterns.LINK_GROUP;
import static com.atlassian.chat.models.specialcontent.SpecialContentPatterns.LINK_PATTERN;
import static com.atlassian.chat.models.specialcontent.SpecialContentPatterns.MENTION_GROUP;
import static com.atlassian.chat.models.specialcontent.SpecialContentPatterns.MENTION_PATTERN;

public enum SpecialContentTypes {
    EMOTICON_TYPE(Pattern.compile(EMOTICON_PATTERN), EMOTICON_GROUP, "emoticons") {
        @Override
        public MinedItems mineFrom(String message) {
            if(message == null || message.isEmpty()) return emptyMinedItems();

            ArrayList<Emoticon> minedItems = new ArrayList<>();
            Matcher matcher = pattern.matcher(message);
            while (matcher.find()) {
                String emoticon = matcher.group(matcherGroup);
                minedItems.add(new Emoticon(emoticon));
            }
            return new MinedItems(minedItems);
        }

        @Override
        protected MinedItems specialItemFromDB(List items) {
            ArrayList<Emoticon> emoticons = new ArrayList<>();
            for (Object emoticon : items) {
                emoticons.add(new Emoticon((String)emoticon));
            }
            return new MinedItems(emoticons);
        }
    },
    MENTION_TYPE(Pattern.compile(MENTION_PATTERN), MENTION_GROUP, "mentions") {
        @Override
        public MinedItems mineFrom(String message) {
            if(message == null || message.isEmpty()) return emptyMinedItems();

            ArrayList<Mention> minedItems = new ArrayList<>();
            Matcher matcher = pattern.matcher(message);
            while (matcher.find()) {
                String mention = matcher.group(matcherGroup);
                minedItems.add(new Mention(mention));
            }
            return new MinedItems(minedItems);
        }

        @Override
        protected MinedItems specialItemFromDB(List items) {
            ArrayList<Mention> mentions = new ArrayList<>();
            for (Object mention : items) {
                mentions.add(new Mention((String)mention));
            }
            return new MinedItems(mentions);
        }
    },
    LINK_TYPE(Pattern.compile(LINK_PATTERN), LINK_GROUP, "links") {
        @Override
        public MinedItems mineFrom(String message) {
            if(message == null || message.isEmpty()) return emptyMinedItems();

            ArrayList<Link> minedItems = new ArrayList<>();
            Matcher matcher = pattern.matcher(message);
            while (matcher.find()) {
                String link = matcher.group(matcherGroup);
                minedItems.add(new Link(link));
            }
            return new MinedItems(minedItems);
        }

        @Override
        protected MinedItems specialItemFromDB(List items) {
            ArrayList<Link> links = new ArrayList<>();
            for (Object linkItem : items) {
                LinkedHashMap linkedHashMap = (LinkedHashMap) linkItem;
                String url = (String) linkedHashMap.get(Link.URL);
                String title = (String) linkedHashMap.get(Link.TITLE);
                Link link = new Link(url);
                link.setPageTitle(title);
                links.add(link);
            }
            return new MinedItems(links);
        }
    };

    public final Pattern pattern;
    public final int matcherGroup;
    public final String key;

    SpecialContentTypes(Pattern pattern, int matcherGroup, String key) {
        this.pattern = pattern;
        this.matcherGroup = matcherGroup;
        this.key = key;
    }

    abstract MinedItems mineFrom(String message);

    @NonNull
    private static MinedItems emptyMinedItems() {
        return new MinedItems(Collections.EMPTY_LIST);
    }

    public static Map<String, MinedItems> mine(String message) {
        if(message == null || message.isEmpty()) return null;
        HashMap<String, MinedItems> minedItemsMap = new HashMap<>();
        for (SpecialContentTypes type : values()) {
            MinedItems minedItems = type.mineFrom(message);
            if(minedItems.hasItems()) minedItemsMap.put(type.key, minedItems);
        }
        return minedItemsMap;
    }

    public static Map<String, MinedItems> createSpecialContentFromDB(String jsonString) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        TypeReference<HashMap<String, Object>> typeReference = new TypeReference<HashMap<String, Object>>() {};
        HashMap<String, Object> itemsMap = objectMapper.readValue(jsonString, typeReference);
        HashMap<String, MinedItems> minedItemsMapFromDB = new HashMap<>();
        SpecialContentTypes[] values = values();
        for (SpecialContentTypes type : values) {
            if(itemsMap.containsKey(type.key)) {
                MinedItems minedItems = type.specialItemFromDB((List) itemsMap.get(type.key));
                minedItemsMapFromDB.put(type.key, minedItems);
            }
        }
        return minedItemsMapFromDB;
    }

    protected abstract MinedItems specialItemFromDB(List items);
}
