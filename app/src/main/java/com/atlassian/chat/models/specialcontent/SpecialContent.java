package com.atlassian.chat.models.specialcontent;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static com.atlassian.chat.models.specialcontent.SpecialContentTypes.LINK_TYPE;

public class SpecialContent {
    private final String chatMessageId;
    private Map<String, MinedItems> itemsMap;

    public SpecialContent(String chatMessageId, Map<String, MinedItems> itemsMap) {
        this.chatMessageId = chatMessageId;
        this.itemsMap = itemsMap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpecialContent that = (SpecialContent) o;

        return chatMessageId.equals(that.chatMessageId) && itemsMap.equals(that.itemsMap);

    }

    @Override
    public int hashCode() {
        int result = chatMessageId.hashCode();
        result = 31 * result + itemsMap.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "SpecialContent{" +
                "itemsMap=" + itemsMap +
                '}';
    }

    public boolean hasLinks() {
        String linkTypeKey = LINK_TYPE.key;
        return itemsMap.containsKey(linkTypeKey) && itemsMap.get(linkTypeKey).hasItems();
    }

    public List<Link> getLinks() {
        return itemsMap.get(LINK_TYPE.key).getMinedItems();
    }

    public void updateLinksWithPageTitles(List<Link> links) {
        itemsMap.put(LINK_TYPE.key, new MinedItems(links));
    }

    public String toJsonString() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(itemsMap);
    }

    public static SpecialContent fromJsonString(String jsonString, String messageId) throws IOException {
        Map<String, MinedItems> itemsMap = SpecialContentTypes.createSpecialContentFromDB(jsonString);
        return new SpecialContent(messageId, itemsMap);
    }
}
