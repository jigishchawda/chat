package com.atlassian.chat.models.specialcontent;

public final class SpecialContentPatterns {
    public static final String EMOTICON_PATTERN = "\\(([a-zA-Z0-9]{1,15})\\)";
    public static final String LINK_PATTERN = "(http[s]?://[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|])";
    public static final String MENTION_PATTERN = "@(\\w+)";
    public static final String TITLE_PATTERN = "<title>(.*)</title>";

    public static final int MENTION_GROUP = 1;
    public static final int LINK_GROUP = 1;
    public static final int EMOTICON_GROUP = 1;

    public static final int TITLE_GROUP = 1;

}
