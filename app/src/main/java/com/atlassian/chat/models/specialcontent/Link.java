package com.atlassian.chat.models.specialcontent;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializable;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class Link extends SpecialItem implements Parcelable, JsonSerializable {
    public static final String LINKS_EXTRAS = "com.atlassian.chat.models.specialcontent.links";
    public static final String URL = "url";
    public static final String TITLE = "title";
    private final String urlString;
    private String pageTitle;

    public Link(String urlString) {
        this.urlString = urlString;
    }

    protected Link(Parcel in) {
        urlString = in.readString();
        pageTitle = in.readString();
    }

    public static final Creator<Link> CREATOR = new Creator<Link>() {
        @Override
        public Link createFromParcel(Parcel in) {
            return new Link(in);
        }

        @Override
        public Link[] newArray(int size) {
            return new Link[size];
        }
    };

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(urlString);
        dest.writeString(pageTitle);
    }

    public URL getUrl() throws MalformedURLException {
        return new URL(urlString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Link link = (Link) o;

        return urlString.equals(link.urlString);

    }

    @Override
    public int hashCode() {
        return urlString.hashCode();
    }

    @Override
    public String toString() {
        return "Link{" +
                "urlString='" + urlString + '\'' +
                ", pageTitle='" + pageTitle + '\'' +
                '}';
    }

    @Override
    public void serialize(JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartObject();
        gen.writeStringField(URL, urlString);
        gen.writeStringField(TITLE, (pageTitle != null) ? pageTitle : "NA");
        gen.writeEndObject();
    }

    @Override
    public void serializeWithType(JsonGenerator gen, SerializerProvider serializers, TypeSerializer typeSer) throws IOException {

    }
}
