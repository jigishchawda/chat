package com.atlassian.chat.models;

import com.atlassian.chat.models.specialcontent.Link;
import com.atlassian.chat.models.specialcontent.MinedItems;
import com.atlassian.chat.models.specialcontent.SpecialContent;
import com.atlassian.chat.models.specialcontent.SpecialContentTypes;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ChatMessage {
    public static final String OUTGOING_MESSAGE_ID_EXTRAS = "com.atlassian.chat.models.chatmessage.outgoing.id";
    public static final String INCOMING_MESSAGE_ID_EXTRAS = "com.atlassian.chat.models.chatmessage.incoming.id";



    public final String id;
    private final SpecialContent specialContent;
    public final String message;
    public final MessageType messageType;
    public final long timestamp;

    private ChatMessage(String messageId, String message, String timestampString, int messageTypeOrdinal, String specialContentJsonString) throws IOException {
        this.id = messageId;
        this.message = message;
        this.timestamp = Long.parseLong(timestampString);
        this.messageType = MessageType.typeByOrdinal(messageTypeOrdinal);
        this.specialContent = specialContentFromJsonString(specialContentJsonString, messageId);
    }

    public ChatMessage(String message, MessageType type, long timestamp) {
        if(message == null || type == null || message.isEmpty())
            throw new IllegalArgumentException("Message or message type cannot be null");
        this.message = message;
        this.messageType = type;
        this.timestamp = timestamp;
        this.id = uniqueId(message, type, timestamp);
        this.specialContent = mineSpecialContent();
    }

    public SpecialContent getSpecialContent() {
        return specialContent;
    }

    public int getMessageType() {
        return messageType.ordinal();
    }

    public void updateLinksWithPageTitles(List<Link> links) {
        specialContent.updateLinksWithPageTitles(links);
    }

    public boolean hasLinks() {
        return specialContent.hasLinks();
    }


    public String getSpecialContentJsonString() {
        try {
            return specialContent.toJsonString();
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    public static ChatMessage fromDBValues(String id, String message, String timestampString, int messageTypeOrdinal, String specialContentJsonString) {
        ChatMessage chatMessage = null;
        try {
            chatMessage = new ChatMessage(id, message, timestampString, messageTypeOrdinal, specialContentJsonString);
        } catch (IOException e) {
            System.out.println("Something is wrong in the DB values.");
        }
        return chatMessage;
    }

    private SpecialContent specialContentFromJsonString(String jsonString, String messageId) throws IOException {
        return SpecialContent.fromJsonString(jsonString, messageId);
    }

    private String uniqueId(String message, MessageType type, long timestamp) {
        return String.valueOf(message.hashCode()) + type.ordinal() + timestamp;
    }


    private SpecialContent mineSpecialContent() {
        Map<String, MinedItems> minedItemsMap = SpecialContentTypes.mine(message);
        return new SpecialContent(id, minedItemsMap);
    }

    public List<Link> getLinks() {
        return specialContent.getLinks();
    }

    public static ChatMessage createIncomingMessage(String message) {
        return new ChatMessage(message, MessageType.INCOMING_MESSAGE, new Date().getTime());
    }

    public static ChatMessage createOutgoingMessage(String message) {
        return new ChatMessage(message, MessageType.OUTGOING_MESSAGE, new Date().getTime());
    }

    public enum MessageType {
        INCOMING_MESSAGE, OUTGOING_MESSAGE;
        public static MessageType typeByOrdinal(int ordinal) {
            MessageType[] values = values();
            for (MessageType type : values) {
                if(type.ordinal() == ordinal) return type;
            }
            return null;
        }
    }

}
