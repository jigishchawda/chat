package com.atlassian.chat.models.specialcontent;

import android.support.annotation.Nullable;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.chat.models.specialcontent.SpecialContentPatterns.TITLE_GROUP;
import static com.atlassian.chat.models.specialcontent.SpecialContentPatterns.TITLE_PATTERN;

public class PageTitleMiner {

    public static String mine(String htmlString) {
        if(htmlString == null) return null;
        Pattern pattern = Pattern.compile(TITLE_PATTERN, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        return mine(pattern, htmlString);
    }

    @Nullable
    private static String mine(Pattern pattern, String htmlString) {
        Matcher matcher = pattern.matcher(htmlString);
        if(matcher.find()) {
            return matcher.group(TITLE_GROUP);
        }
        return null;
    }
}
