package com.atlassian.chat.models.network;

import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContentType {
    private static final Pattern CHARSET_HEADER = Pattern.compile("charset=([-_a-zA-Z0-9]+)", Pattern.CASE_INSENSITIVE|Pattern.DOTALL);
    private static final int CHARSET_GROUP = 1;

    private String value;
    private String charsetName;
    public ContentType(String headerValue) {
        if (headerValue == null)
            throw new IllegalArgumentException("ContentType must be constructed with a non-null headerValue");

        int semiColonOffset = headerValue.indexOf(";");
        if (semiColonOffset != -1) {
            value = headerValue.substring(0, semiColonOffset);
            Matcher matcher = CHARSET_HEADER.matcher(headerValue);
            if (matcher.find())
                charsetName = matcher.group(CHARSET_GROUP);
        }
        else
            value = headerValue;
    }

    public Charset getCharset() {
        if (charsetName != null && Charset.isSupported(charsetName))
            return Charset.forName(charsetName);
        else
            return Charset.defaultCharset();

    }

    public boolean is(String expectedContentType) {
        return value != null && value.equals(expectedContentType);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContentType that = (ContentType) o;

        if (!value.equals(that.value)) return false;
        return !(charsetName != null ? !charsetName.equals(that.charsetName) : that.charsetName != null);

    }

    @Override
    public int hashCode() {
        int result = value.hashCode();
        result = 31 * result + (charsetName != null ? charsetName.hashCode() : 0);
        return result;
    }
}
