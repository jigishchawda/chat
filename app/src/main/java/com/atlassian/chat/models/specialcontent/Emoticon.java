package com.atlassian.chat.models.specialcontent;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializable;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;

import java.io.IOException;

public class Emoticon extends SpecialItem implements JsonSerializable{
    private final String emoticon;

    public Emoticon(String emoticon) {
        this.emoticon = emoticon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Emoticon emoticon1 = (Emoticon) o;

        return emoticon.equals(emoticon1.emoticon);

    }

    @Override
    public int hashCode() {
        return emoticon.hashCode();
    }

    @Override
    public String toString() {
        return "Emoticon{" +
                "emoticon='" + emoticon + '\'' +
                '}';
    }

    @Override
    public void serialize(JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeObject(emoticon);
    }

    @Override
    public void serializeWithType(JsonGenerator gen, SerializerProvider serializers, TypeSerializer typeSer) throws IOException {

    }
}
