package com.atlassian.chat.models.specialcontent;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializable;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;

import java.io.IOException;

public class Mention extends SpecialItem implements JsonSerializable{
    private final String mention;

    public Mention(String mention) {
        this.mention = mention;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Mention mention1 = (Mention) o;

        return mention.equals(mention1.mention);

    }

    @Override
    public int hashCode() {
        return mention.hashCode();
    }

    @Override
    public String toString() {
        return "Mention{" +
                "mention='" + mention + '\'' +
                '}';
    }

    @Override
    public void serialize(JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeObject(mention);
    }

    @Override
    public void serializeWithType(JsonGenerator gen, SerializerProvider serializers, TypeSerializer typeSer) throws IOException {

    }
}
