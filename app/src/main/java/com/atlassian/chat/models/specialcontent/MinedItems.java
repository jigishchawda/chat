package com.atlassian.chat.models.specialcontent;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializable;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;

import java.io.IOException;
import java.util.List;

public class MinedItems<I extends SpecialItem> implements JsonSerializable {
    protected List<I> minedItems;

    public MinedItems(List<I> minedItems) {
        this.minedItems = minedItems;
    }

    public List<I> getMinedItems() {
        return minedItems;
    }

    public boolean hasItems() {
        return minedItems.size() > 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MinedItems<?> that = (MinedItems<?>) o;

        return minedItems.equals(that.minedItems);

    }

    @Override
    public int hashCode() {
        return minedItems.hashCode();
    }

    @Override
    public String toString() {
        return "MinedItems{" +
                "minedItems=" + minedItems +
                '}';
    }

    @Override
    public void serialize(JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartArray(minedItems.size());
        for (I item : minedItems) {
            gen.writeObject(item);
        }
        gen.writeEndArray();

    }

    @Override
    public void serializeWithType(JsonGenerator gen, SerializerProvider serializers, TypeSerializer typeSer) throws IOException {

    }
}
