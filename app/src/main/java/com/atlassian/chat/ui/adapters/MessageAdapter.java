package com.atlassian.chat.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.atlassian.chat.R;
import com.atlassian.chat.models.ChatMessage;
import com.atlassian.chat.models.ChatMessage.MessageType;

import java.util.ArrayList;
import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ChatMessage> messages;
    private MessageType[] messageTypes = MessageType.values();

    public MessageAdapter(List<ChatMessage> messages) {
        this.messages = messages;
    }

    public MessageAdapter() {
        messages = new ArrayList<>();
    }

    @Override
    public int getItemViewType(int position) {
        ChatMessage chatMessage = messages.get(position);
        return chatMessage.getMessageType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        if(viewType == MessageType.INCOMING_MESSAGE.ordinal()) {
            View view = layoutInflater.inflate(R.layout.incoming_message, parent, false);
            viewHolder = new IncomingMessageViewHolder(view);
        } else if(viewType == MessageType.OUTGOING_MESSAGE.ordinal()) {
            View view = layoutInflater.inflate(R.layout.outgoing_message, parent, false);
            viewHolder = new OutgoingMessageViewHolder(view);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof IncomingMessageViewHolder) {
            IncomingMessageViewHolder messageViewHolder = (IncomingMessageViewHolder) holder;
            messageViewHolder.getMessageView().setText(messages.get(position).message);
        } else if (holder instanceof OutgoingMessageViewHolder) {
            OutgoingMessageViewHolder messageViewHolder = (OutgoingMessageViewHolder) holder;
            messageViewHolder.getMessageView().setText(messages.get(position).message);
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }


    public void addMessage(ChatMessage chatMessage) {
        messages.add(chatMessage);
        notifyItemInserted(messages.size() - 1);
    }
}
