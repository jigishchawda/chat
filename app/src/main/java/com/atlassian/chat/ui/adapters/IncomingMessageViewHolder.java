package com.atlassian.chat.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.atlassian.chat.R;

public class IncomingMessageViewHolder extends RecyclerView.ViewHolder {
    private TextView messageTextView;

    public IncomingMessageViewHolder(View itemView) {
        super(itemView);
        messageTextView = (TextView) itemView.findViewById(R.id.message);
    }

    public TextView getMessageView() {
        return messageTextView;
    }
}
