package com.atlassian.chat.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.atlassian.chat.R;

public class OutgoingMessageViewHolder extends RecyclerView.ViewHolder {
    private TextView messageTextView;

    public OutgoingMessageViewHolder(View itemView) {
        super(itemView);
        messageTextView = (TextView) itemView.findViewById(R.id.message);
    }

    public TextView getMessageView() {
        return messageTextView;
    }
}
