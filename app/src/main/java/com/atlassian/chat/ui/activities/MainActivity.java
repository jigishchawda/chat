package com.atlassian.chat.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.atlassian.chat.IHTMLPageTitleFetcher;
import com.atlassian.chat.PageTitleFetcherIntentService;
import com.atlassian.chat.R;
import com.atlassian.chat.db.DatabaseResultCallback;
import com.atlassian.chat.db.operations.MessageAddAsyncTask;
import com.atlassian.chat.db.operations.MessageFetchByIdAsyncTask;
import com.atlassian.chat.db.operations.MessagesCountAsynTask;
import com.atlassian.chat.db.operations.MessagesFetchAsyncTask;
import com.atlassian.chat.models.ChatMessage;
import com.atlassian.chat.models.specialcontent.Link;
import com.atlassian.chat.ui.adapters.MessageAdapter;
import com.atlassian.chat.ui.adapters.MessagesVerticalSpacingItemDecoration;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.chat.models.ChatMessage.INCOMING_MESSAGE_ID_EXTRAS;
import static com.atlassian.chat.models.ChatMessage.OUTGOING_MESSAGE_ID_EXTRAS;
import static com.atlassian.chat.models.specialcontent.Link.LINKS_EXTRAS;
import static com.atlassian.chat.util.NetworkUtil.isConnectedToInternet;

public class MainActivity extends AppCompatActivity implements IHTMLPageTitleFetcher {
    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String incomingMessageId = intent.getStringExtra(INCOMING_MESSAGE_ID_EXTRAS);
            new MessageFetchByIdAsyncTask(MainActivity.this, new DatabaseResultCallback<ChatMessage>() {
                @Override
                public void execute(ChatMessage incomingMessage) {
                    if (incomingMessage != null) {
                        renderOnView(incomingMessage);
                    }
                }
            }).execute(incomingMessageId);
        }
    };

    private RecyclerView messagesView;
    private EditText messageEditTextView;
    private MessageAdapter messageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        messageEditTextView = (EditText) findViewById(R.id.text_message);
        messagesView = (RecyclerView) findViewById(R.id.messages);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        messagesView.setLayoutManager(linearLayoutManager);
        messagesView.addItemDecoration(new MessagesVerticalSpacingItemDecoration());
        renderMessagesFromDatabase();

    }


    private void renderMessagesFromDatabase() {
        new MessagesFetchAsyncTask(this, new DatabaseResultCallback<List<ChatMessage>>() {
            @Override
            public void execute(List<ChatMessage> result) {
                messageAdapter = new MessageAdapter(result);
                messagesView.setAdapter(messageAdapter);
                messagesView.scrollToPosition(result.size() - 1);
            }
        }).execute();
    }

    private void startService(List<Link> links, String messageId) {
        Intent intent = new Intent(this, PageTitleFetcherIntentService.class);
        intent.putParcelableArrayListExtra(LINKS_EXTRAS, (ArrayList<? extends Parcelable>) links);
        intent.putExtra(OUTGOING_MESSAGE_ID_EXTRAS, messageId);

        startService(intent);
    }


    @Override
    public void fetchPageTitlesFor(List<Link> links, String messageId) {
        if (!isConnectedToInternet(this)) {
            showNotConnectedToInternetToast();
        }
        startService(links, messageId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(PageTitleFetcherIntentService.PAGE_TITLES_FETCHED));
        renderNewMessages();
    }



    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    public void sendMessage(View view) {
        String message = messageEditTextView.getText().toString();
        if (message.isEmpty()) return;

        clearMessageBox();
        ChatMessage chatMessage = ChatMessage.createOutgoingMessage(message);
        renderOnView(chatMessage);
        new MessageAddAsyncTask(this).execute(chatMessage);
        if (chatMessage.hasLinks()) {
            fetchPageTitlesFor(chatMessage.getLinks(), chatMessage.id);
        } else {
            ChatMessage incomingMessage = ChatMessage.createIncomingMessage(chatMessage.getSpecialContentJsonString());
            new MessageAddAsyncTask(this).execute(incomingMessage);
            renderOnView(incomingMessage);
        }

    }

    private void renderOnView(ChatMessage chatMessage) {
        messageAdapter.addMessage(chatMessage);
        messagesView.scrollToPosition(messageAdapter.getItemCount() - 1);
    }

    private void clearMessageBox() {
        messageEditTextView.setText("");
    }

    private void showNotConnectedToInternetToast() {
        Toast.makeText(MainActivity.this, "Cannot fetch page titles. Please connect to internet.", Toast.LENGTH_SHORT).show();
    }

    private void renderNewMessages() {
        new MessagesCountAsynTask(this, new DatabaseResultCallback<Long>() {
            @Override
            public void execute(Long messagesCount) {
                if(messagesCount > messageAdapter.getItemCount()) renderMessagesFromDatabase();
            }
        }).execute();
    }
}
