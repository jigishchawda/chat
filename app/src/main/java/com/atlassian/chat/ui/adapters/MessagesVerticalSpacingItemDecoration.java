package com.atlassian.chat.ui.adapters;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class MessagesVerticalSpacingItemDecoration extends RecyclerView.ItemDecoration {

    public static final int VERTICAL_SPACING = 20;

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        outRect.bottom = VERTICAL_SPACING;
    }
}