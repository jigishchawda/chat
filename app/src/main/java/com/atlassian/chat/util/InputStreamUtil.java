package com.atlassian.chat.util;

import android.support.annotation.NonNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public class InputStreamUtil {

    @NonNull
    public static String readFromStream(InputStream in, Charset charset) throws IOException {
        int length;
        final int BUFFER_SIZE = 4 * 1024;
        int eof = -1;
        int startOffset = 0;
        StringBuilder content = new StringBuilder();
        char[] buf = new char[BUFFER_SIZE];

        BufferedReader reader = new BufferedReader(new InputStreamReader(in, charset));
        // read until EOF or first 8192 characters
        while ((length = reader.read(buf)) != eof) {
            content.append(buf, startOffset, length);
        }
        reader.close();
        return content.toString();
    }

}
