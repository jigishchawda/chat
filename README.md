# README #
This is the test android app which allows user to input message and the bot will extract special content and display it in json string

### What is this repository for? ###

The assignment asks to accept a user input and return a json string with special content like links, emoticons and mentions in a certain format. There are assumptions made on the Android Application front, since no specific requirements were asked for.

When the user installs/opens the app for the first time, a chat-messenger-kind-of screen is displayed, with an input box and a send button on its right. The user is expected to input test strings(messages) in the input box and press send button. The program extracts special content and the displays it as a reply (incoming message).

The test inputs along with their special content persist in the database. Hence opening the app after the first time, will display you the previous test inputs. The application will display the special content, if not displayed, when the application comes to foreground.

![Screen Shot 2016-01-17 at 11.33.02 PM.png](https://bitbucket.org/repo/a6g8KM/images/32409155-Screen%20Shot%202016-01-17%20at%2011.33.02%20PM.png)



### How do I get set up? ###


Clone the repo on local machine. Import the project in Android studio. Install necessary build tools upon prompt.

* Libraries used

    Jackson - for json conversion.

* How to run tests
 
    execute in commandline from root directory >> `./gradlew test`

+ Deployment instructions 
    * `./gradlew assembleDebug`
    * `adb install app/build/outputs/apk/app-debug.apk` #(from root folder)
    
    OR

    * `./gradlew installDebug` (requires atleast one connected emulator or device)
    * open the app manually from the connected device.

#### Please note: The app has been tested on Nexus 4 emulator and a Nexus 4 device running Android 5.0.